from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from .views import index
from homepage.apps import HomepageConfig
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class TestUnit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_app(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_title_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Story 8", response_content)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self):
        self.browser.quit()
    
    def test_button_to_change_accordion_order_up(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        button = self.browser.find_element_by_id('up2')
        button.click()
        time.sleep(2)

        newAccordion = self.browser.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][1]")
        self.assertEqual("a2", newAccordion.get_attribute("id"))

    def test_button_to_change_accordion_order_down(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        button = self.browser.find_element_by_id('down1')
        button.click()
        time.sleep(2)

        newAccordion = self.browser.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][2]")
        self.assertEqual("a1", newAccordion.get_attribute("id"))